import { Category } from "./category";
import { Brand } from "./brand";

export class Product {
    md5: string;
    image: string;
    id: number;
    name: string;
    description: string;
    brand: Brand
    categories: [
        Array<Category>
    ]
}