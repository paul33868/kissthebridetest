import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Category } from '../../models/category';
import { DataProvider } from '../../providers/data/data';
import { Brand } from '../../models/brand';
import { Product } from '../../models/product';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  private title: string;
  private buttonText: string;
  private product: FormGroup;
  private productToEdit: Product;
  private categories: Array<Category>;
  private brands: Array<Brand>;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private dataProvider: DataProvider,
    public toastCtrl: ToastController) {
    this.productToEdit = this.navParams.get('product');
    this.title = this.productToEdit ? this.productToEdit.name : 'New product';
    this.buttonText = this.productToEdit ? 'Edit' : 'Add';
    let productName = this.productToEdit ? this.productToEdit.name : '';
    let productDescription = this.productToEdit ? this.productToEdit.description : '';
    let productURL = this.productToEdit ? this.productToEdit.image : '';
    let productCategories;
    if (this.productToEdit) {
      let categories: any = [];
      this.productToEdit.categories.forEach(category => {
        categories.push(category['id']);
      });
      productCategories = categories;
    }
    else {
      productCategories = '';
    }
    let productBrand = this.productToEdit ? this.productToEdit.brand.id : '';
    this.product = new FormGroup({
      name: new FormControl(productName, Validators.required),
      description: new FormControl(productDescription, Validators.required),
      url: new FormControl(productURL),
      categories: new FormControl(productCategories),
      brand: new FormControl(productBrand, Validators.required)
    });
    this.getCategories();
    this.getBrands();
  }

  getCategories() {
    this.dataProvider.getCategories()
      .subscribe(
      (data: Array<Category>) => {
        this.categories = data
      },
      error => {
        console.error(error);
      });
  }

  getBrands() {
    this.dataProvider.getBrands()
      .subscribe(
      (data: Array<Brand>) => {
        this.brands = data
      },
      error => {
        console.error(error);
      });
  }

  addOrEditProduct() {
    if (this.productToEdit) {
      this.product.value['id'] = this.productToEdit.id;
      this.dataProvider.editProduct(this.product.value)
        .subscribe(
        data => {
          let toast = this.toastCtrl.create({
            message: 'Product ' + data.name + ' was edited successfully',
            duration: 3000
          });
          toast.present();
          this.navCtrl.pop();
        },
        error => {
          console.error(error);
        });
    }
    else {
      this.dataProvider.addProduct(this.product.value)
        .subscribe(
        data => {
          let toast = this.toastCtrl.create({
            message: 'Product ' + data.name + ' was added successfully',
            duration: 3000
          });
          toast.present();
          this.navCtrl.pop();
        },
        error => {
          console.error(error);
        });
    }
  }
}
