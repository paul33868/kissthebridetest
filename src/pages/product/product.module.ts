import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductPage } from './product';
import { DataProvider } from '../../providers/data/data';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    ProductPage,
  ],
  imports: [
    HttpModule,
    IonicPageModule.forChild(ProductPage),
  ],
  exports: [
    ProductPage
  ],
  providers: [
    DataProvider
  ]
})
export class ProductPageModule { }
