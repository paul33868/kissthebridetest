import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Product } from '../../models/product';
import { DataProvider } from '../../providers/data/data';
import { ProductPage } from '../product/product';
import { Category } from '../../models/category';
import { Brand } from '../../models/brand';


@Component({
  selector: 'page-home',
  templateUrl: './home.html'
})
export class HomePage {
  private products: Array<Product>;
  private totalProducts: Array<Product>;
  private currentPage: number = 1;
  private pageSize: number = 5;
  private totalPages: number;
  private searchedValue: string;
  private categories: Array<Category>;
  private brands: Array<Brand>;
  constructor(
    private navCtrl: NavController,
    private dataProvider: DataProvider) { }

  ionViewWillEnter() {
    this.getProducts();
    this.getCategories();
    this.getBrands();
  }

  // In order to get the total amount of products I needed to perform this call.
  // Perhaps it's better to make an end-point with the total amount of records.
  getProducts() {
    this.searchedValue = '';
    this.dataProvider.getProducts(this.currentPage - 1)
      .subscribe(
      (data: Array<Product>) => {
        this.totalPages = Math.floor(data.length / this.pageSize) + 1
        this.products = data.filter((item, pageToSearch) => pageToSearch < this.pageSize);
        // This is a quick fix in order to bypass the filtering
        this.totalProducts = data;
      },
      error => {
        console.error(error);
      });
  }

  filterByCategory(selectedCategories) {
    // this.totalProducts = this.totalProducts.filter((item) => item.brand === selectedBrand);
  }

  filterByBrand(selectedBrand) {
    console.log(selectedBrand);
    let filteredProducts = [];
    this.totalProducts.forEach(product => {
      if (product.brand && product.brand.id == Number(selectedBrand)) {
        filteredProducts.push(product);
      }
    });
    this.products = filteredProducts.filter((item, pageToSearch) => pageToSearch < this.pageSize);
  }

  getProductsByPage(page?: number) {
    this.currentPage = page;
    this.dataProvider.getProducts((this.currentPage - 1) * this.pageSize, this.pageSize)
      .subscribe(
      (data: Array<Product>) => {
        this.products = data;
      },
      error => {
        console.error(error);
      });
  }

  searchProduct() {
    if (this.searchedValue && this.searchedValue.trim() != '') {
      this.dataProvider.getProducts(0, this.pageSize, this.searchedValue)
        .subscribe(
        (data: Array<Product>) => {
          this.totalPages = Math.floor(data.length / this.pageSize) + 1
          this.products = data.filter((item, pageToSearch) => pageToSearch < this.pageSize);
        },
        error => {
          console.error(error);
        });
    }
  }

  addProduct() {
    this.navCtrl.push(ProductPage);
  }

  editProduct(product: Product) {
    this.navCtrl.push(ProductPage, { product: product });
  }

  getCategories() {
    this.dataProvider.getCategories()
      .subscribe(
      (data: Array<Category>) => {
        this.categories = data
      },
      error => {
        console.error(error);
      });
  }

  getBrands() {
    this.dataProvider.getBrands()
      .subscribe(
      (data: Array<Brand>) => {
        this.brands = data
      },
      error => {
        console.error(error);
      });
  }
}
