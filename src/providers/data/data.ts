import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { API_URL } from '../../environments/environment';
import { Product } from '../../models/product';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {

  constructor(public http: Http) { }

  getProducts(start, limit?, search?) {
    let limitNumber = Number(limit) ? '&limit=' + limit : '';
    let searchValue = search ? '&search=' + search : '';
    return this.http.get(API_URL + 'products' + '?_format=json&start=' + start + limitNumber + searchValue)
      .map((res => res.json()))
  }

  getCategories() {
    return this.http.get(API_URL + 'categories?_format=json')
      .map((res => res.json()))
  }

  getBrands() {
    return this.http.get(API_URL + 'brands?_format=json')
      .map((res => res.json()))
  }

  addProduct(product: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = {
      name: product.name,
      description: product.description,
      url: product.image,
      categories: product.categories,
      brand: product.brand
    }

    return this.http.post(
      API_URL + 'products',
      body,
      options)
      .map(res => res.json());
  }

  editProduct(product: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let body = {
      name: product.name,
      description: product.description,
      url: product.image,
      categories: product.categories,
      brand: product.brand
    }

    return this.http.put(
      API_URL + 'products/' + product.id,
      body,
      options)
      .map(res => res.json());
  }
}