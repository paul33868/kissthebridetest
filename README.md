README - kissTheBrideTest
## How to set the enviroment

1- Install ionic:

```bash
$ npm install -g ionic cordova
```

2- Clone the project:

```bash
$ git clone https://paul33868@bitbucket.org/paul33868/kissthebridetest.git
```

3- Go to the project folder and install the packages

```bash
$ npm install
```

### Lunch project (on the project folder):

To lunch the project:

```bash
$ ionic serve
```

To run the test:

```bash
$ ng test
```

To run the test coverage:

```bash
$ ionic serve
```
To see the cove coverage go to the project's root folder/coverage and open index.html